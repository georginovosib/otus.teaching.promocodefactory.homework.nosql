﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Otus.Teaching.Pcf.Administration.Core.Domain
{
    public class MongoBaseEntity
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }
    }
}
