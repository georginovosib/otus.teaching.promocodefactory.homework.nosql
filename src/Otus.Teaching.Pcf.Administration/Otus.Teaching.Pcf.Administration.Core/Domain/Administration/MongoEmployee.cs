﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Otus.Teaching.Pcf.Administration.Core.Domain.Administration
{
    [Table("MongoEmployees")]
    public class MongoEmployee : MongoBaseEntity
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; set; }

        public int AppliedPromocodesCount { get; set; }

        public string RoleId { get; set; }
    }
}
