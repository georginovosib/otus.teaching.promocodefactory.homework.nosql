﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Otus.Teaching.Pcf.Administration.Core.Domain.Administration
{
    [Table("MongoRoles")]
    public class MongoRole : MongoBaseEntity
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}
