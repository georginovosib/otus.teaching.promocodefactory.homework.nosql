﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories
{
    public interface IMongoRepository<T> where T : MongoBaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();

        Task<T> GetByIdAsync(string id);

        Task CreateAsync(T t);

        Task DeleteAsync(string id);

        Task UpdateAsync(string id, T t);
    }
}
