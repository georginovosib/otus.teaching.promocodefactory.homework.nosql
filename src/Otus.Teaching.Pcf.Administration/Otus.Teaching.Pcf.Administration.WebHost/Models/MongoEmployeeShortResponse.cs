﻿using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

namespace Otus.Teaching.Pcf.Administration.WebHost.Models
{
    public class MongoEmployeeShortResponse
    {
        public MongoEmployeeShortResponse()
        {
        }

        public MongoEmployeeShortResponse(MongoEmployee mongoEmployee)
        {
            _id = mongoEmployee._id;
            FullName = mongoEmployee.FullName;
            Email = mongoEmployee.Email;
        }

        public string _id { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
    }
}
