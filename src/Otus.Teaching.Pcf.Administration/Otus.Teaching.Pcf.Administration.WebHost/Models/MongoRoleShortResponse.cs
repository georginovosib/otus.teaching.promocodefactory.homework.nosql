﻿using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

namespace Otus.Teaching.Pcf.Administration.WebHost.Models
{
    public class MongoRoleShortResponse
    {
        public MongoRoleShortResponse()
        {
        }

        public MongoRoleShortResponse(MongoRole role)
        {
            Name = role.Name;
        }
        public string _id { get; set; }
        public string Name { get; set; }
    }
}
