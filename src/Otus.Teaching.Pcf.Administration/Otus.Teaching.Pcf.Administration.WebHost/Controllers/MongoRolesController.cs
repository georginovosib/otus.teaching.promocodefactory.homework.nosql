﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class MongoRolesController : ControllerBase
    {
        private readonly IMongoRepository<MongoRole> _mongoRoleRepository;

        public MongoRolesController(IMongoRepository<MongoRole> mongoRoleRepository)
        {
            _mongoRoleRepository = mongoRoleRepository;
        }

        /// <summary>
        /// Получить все роли
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<MongoRoleShortResponse>> GetMongoRolesAsync()
        {
            var roles = await _mongoRoleRepository.GetAllAsync();

            var rolesModelList = roles.Select(x =>
                new MongoRoleShortResponse()
                {
                    _id = x._id,
                    Name = x.Name
                }).ToList();

            return rolesModelList;
        }

        /// <summary>
        /// Получить роль по id
        /// </summary>
        /// <param name="id"><example>507f1f77bcf86cd799439011</example></param>
        /// <returns></returns>
        [HttpGet("{id:length(24)}")]
        public async Task<ActionResult<MongoRoleShortResponse>> GetMongoRoleByIdAsync(string id)
        {
            var mongoRole = await _mongoRoleRepository.GetByIdAsync(id);

            if (mongoRole == null) return NotFound();

            var roleModel = new MongoRoleShortResponse(mongoRole);

            return roleModel;
        }

        /// <summary>
        /// Добавить роль
        /// </summary>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateMongoRoleAsync(string name, string description)
        {
            MongoRole mongoRole = new MongoRole() { Name = name, Description = description };

            await _mongoRoleRepository.CreateAsync(mongoRole);

            return Ok("Добавлена роль");
        }

        /// <summary>
        /// Удалить роль по id
        /// </summary>
        /// <param name="id"><example>507f1f77bcf86cd799439012</example></param>
        /// <returns></returns>
        [HttpDelete("{id:length(24)}")]
        public async Task<ActionResult> DeleteMongoRoleAsync(string id)
        {
            await _mongoRoleRepository.DeleteAsync(id);

            return Ok("Роль удалена");
        }

        /// <summary>
        /// Обновить роль по Id
        /// </summary>
        /// <param name="id"><example>507f1f77bcf86cd799439012</example></param>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        [HttpPost("Update/{id:length(24)}")]
        public async Task<ActionResult> UpdateMongoRoleAsync(string id, string name, string description)
        {
            var mongoRole = await _mongoRoleRepository.GetByIdAsync(id);

            if (mongoRole == null) return NotFound("Роли с таким id не существует");

            if (name != null) mongoRole.Name = name;

            if (description != null) mongoRole.Description = description;

            await _mongoRoleRepository.UpdateAsync(id, mongoRole);

            return Ok("Данные обновлены");
        }
    }
}
