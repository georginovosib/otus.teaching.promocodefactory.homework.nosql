﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.WebHost.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class MongoEmployeesController : ControllerBase
    {
        private readonly IMongoRepository<MongoEmployee> _mongoEmployeeRepository;

        public MongoEmployeesController(IMongoRepository<MongoEmployee> mongoEmployeeRepository)
        {
            _mongoEmployeeRepository = mongoEmployeeRepository;
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<MongoEmployeeShortResponse>> GetMongoEmployeesAsync()
        {
            var employees = await _mongoEmployeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x =>
                new MongoEmployeeShortResponse()
                {
                    _id = x._id,
                    Email = x.Email,
                    FullName = x.FullName,
                }).ToList();

            return employeesModelList;
        }

        /// <summary>
        /// Получить данные сотрудника по id
        /// </summary>
        /// <param name="id"><example>629c4c0d37419f8c9a3b9894</example></param>
        /// <returns></returns>
        [HttpGet("{id:length(24)}")]
        public async Task<ActionResult<MongoEmployeeShortResponse>> GetMongoEmployeeByIdAsync(string id)
        {
            var mongoEmployee = await _mongoEmployeeRepository.GetByIdAsync(id);

            if (mongoEmployee == null) return NotFound();

            var employeeModel = new MongoEmployeeShortResponse(mongoEmployee);

            return employeeModel;
        }

        /// <summary>
        /// Добавить сотрудника
        /// </summary>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateMongoEmployeeAsync(string firstName, string lastName, string email)
        {
            MongoEmployee mongoEmployee = new MongoEmployee() { FirstName = firstName, LastName = lastName, Email = email };

            await _mongoEmployeeRepository.CreateAsync(mongoEmployee);

            return Ok("Добавлен сотрудник");
        }

        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        /// <param name="id"><example>629c4f2a666f462c5bb8429b</example></param>
        /// <returns></returns>
        [HttpDelete("{id:length(24)}")]
        public async Task<ActionResult<EmployeeResponse>> DeleteEmployeeAsync(string id)
        {
            await _mongoEmployeeRepository.DeleteAsync(id);

            return Ok("Сотрудник удален");
        }

        /// <summary>
        /// Обновить данные сотрудника по Id
        /// </summary>
        /// <param name="id"><example>629c5470bc24b4fdecb94f18</example></param>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        [HttpPost("Update/{id:length(24)}")]
        public async Task<ActionResult> UpdateMongoRoleAsync(string id, string firstName, string lastName, string email)
        {
            var mongoEmployee = await _mongoEmployeeRepository.GetByIdAsync(id);

            if (mongoEmployee == null) return NotFound("Сотрудника с таким id не существует");

            if (firstName != null) mongoEmployee.FirstName = firstName;

            if (lastName != null) mongoEmployee.LastName = lastName;

            if (email != null) mongoEmployee.Email = email;

            await _mongoEmployeeRepository.UpdateAsync(id, mongoEmployee);

            return Ok("Данные обновлены");
        }
    }
}
