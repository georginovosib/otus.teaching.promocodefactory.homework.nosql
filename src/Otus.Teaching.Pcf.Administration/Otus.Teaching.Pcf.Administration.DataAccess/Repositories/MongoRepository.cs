﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain;
using Otus.Teaching.Pcf.Administration.DataAccess.Data;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Repositories
{
    public class MongoRepository<T> : IMongoRepository<T> where T : MongoBaseEntity
    {
        public readonly IMongoCollection<T> _mongoCollection;

        public MongoRepository(IOptions<Settings> options)
        {
            var client = new MongoClient(options.Value.ConnectionString);
            var database = client.GetDatabase(options.Value.Database);
            var table = typeof(T).GetCustomAttribute<TableAttribute>(false).Name;
            _mongoCollection = database.GetCollection<T>(table);
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return (await _mongoCollection.FindAsync<T>(a => true)).ToEnumerable();
        }

        public async Task<T> GetByIdAsync(string id)
        {
            return await _mongoCollection.Find(x => x._id == id).FirstOrDefaultAsync();
        }

        public async Task CreateAsync(T t)
        {
           await _mongoCollection.InsertOneAsync(t);
        }

        public async Task DeleteAsync(string id)
        {
            await _mongoCollection.DeleteOneAsync(x => x._id == id);
        }
        public async Task UpdateAsync(string id, T t)
        {
            await _mongoCollection.ReplaceOneAsync(x => x._id == id, t);
        }
    }
}
