﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Data
{
    public class MongoDbInitializer : IMongoDbInitializer
    {
        private IMongoCollection<MongoEmployee> _employeeCollection;
        private IMongoCollection<MongoRole> _roleCollection;
        public MongoDbInitializer(IOptions<Settings> options)
        {
            var client = new MongoClient(options.Value.ConnectionString);
            client.DropDatabase(options.Value.Database);
            var database = client.GetDatabase(options.Value.Database);
            _employeeCollection = database.GetCollection<MongoEmployee>("MongoEmployees");
            _roleCollection = database.GetCollection<MongoRole>("MongoRoles");
        }

        public void InitializeDb()
        {
            _employeeCollection.InsertMany(MongoFakeDataFactory.MongoEmployees);
            _roleCollection.InsertMany(MongoFakeDataFactory.MongoRoles);
        }
    }
}
