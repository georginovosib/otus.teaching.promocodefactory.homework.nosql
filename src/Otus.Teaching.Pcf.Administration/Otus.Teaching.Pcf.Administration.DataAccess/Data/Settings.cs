﻿namespace Otus.Teaching.Pcf.Administration.DataAccess.Data
{
    public class Settings
    {
        public string ConnectionString { get; set; }
        public string Database { get; set; }
    }
}
