﻿using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Driver;
using MongoDB.Bson;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Data
{
    public static class MongoFakeDataFactory
    {
        public static List<MongoEmployee> MongoEmployees => new List<MongoEmployee>()
        {
            new MongoEmployee()
            {
                Email = "owner@somemail.ru",
                FirstName = "Олег",
                LastName = "Андреевич",
                RoleId = "507f1f77bcf86cd799439011",
                AppliedPromocodesCount = 5
            },
            new MongoEmployee()
            {
                Email = "andreev@somemail.ru",
                FirstName = "Мирр",
                LastName = "Иоаннович",
                RoleId = "507f1f77bcf86cd799439012",
                AppliedPromocodesCount = 10
            },
        };

        public static List<MongoRole> MongoRoles => new List<MongoRole>()
        {
            new MongoRole()
            {
                _id = "507f1f77bcf86cd799439011",
                Name = "MongoAdmin",
                Description = "Администратор",
            },
            new MongoRole()
            {
                 _id = "507f1f77bcf86cd799439012",
                Name = "MongoPartnerManager",
                Description = "Партнерский менеджер"
            }
        };
    }
}
