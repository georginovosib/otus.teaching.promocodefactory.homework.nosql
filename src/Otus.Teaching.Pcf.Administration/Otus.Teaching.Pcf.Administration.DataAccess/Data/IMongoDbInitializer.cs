﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Data
{
    public interface IMongoDbInitializer
    {
        void InitializeDb();
    }
}
